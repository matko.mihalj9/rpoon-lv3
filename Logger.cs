﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace lv3
{
    public class Logger
    {
        
        private string filePath;
        private static Logger instance;
        public Logger( string filePath)
        {
          
            this.filePath = filePath;
        }
        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger("/Users/matko/Desktop/matko.csv");
            }
            return instance;
        }
        public String Filepath
        {
            get { return filePath; }
            set { filePath = value; }

        }
        public void Log(string message) 
        {
            
             using (System.IO.StreamWriter fileWriter =
               new System.IO.StreamWriter(this.filePath,true))
                {
                    fileWriter.WriteLine(message);
                }
            
        }
    }
}
