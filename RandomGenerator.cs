﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace lv3
{
    class RandomGenerator
    {
        private static RandomGenerator instance;
        private Random generator;
        private RandomGenerator()
        {
            this.generator = new Random();
        }
        public static RandomGenerator GetInstance()
        {
            if (instance == null)
            {
                instance = new RandomGenerator();
            }
            return instance;
        }
        //public int NextInt()
        //{
        //    return this.generator.Next();
        //}
        //public int NextInt(int upperBound)
        //{
        //    return this.generator.Next(upperBound);
        //}

        //public double NextDouble()
        //{
        //    return this.generator.NextDouble();
        //}

        public int[,] NextInt(int red, int stupac)
        {
            int[,] matrica = new int[red, stupac];
            for (int i = 0; i < red; i++)
            {
                for (int j = 0; j < stupac; j++)
                {
                    matrica[i, j] = generator.Next(1, 5);
                }
            }
          

            return matrica;
        }
    }
}